import sys
import os
import platform
import time
import ctypes

from array import *
from ctypes import *
#from __builtin__ import exit

# LSL imports
from pylsl import StreamInfo, StreamOutlet

if sys.platform.startswith('win32'):
    import msvcrt
elif sys.platform.startswith('linux'):
    import atexit
    from select import select

from ctypes import *

try:
    if sys.platform.startswith('win32'):
        libEDK = cdll.LoadLibrary("bin/win32/edk.dll")
    elif sys.platform.startswith('linux'):
        srcDir = os.getcwd()
        if platform.machine().startswith('arm'):
            libPath = srcDir + "/bin/armhf/libedk.so"
        else:
            libPath = srcDir + "/bin/linux64/libedk.so"
        libEDK = CDLL(libPath)
    else:
        raise Exception('System not supported.')
except Exception as e:
    print('Error: cannot load EDK lib:', e)
    exit()

print("away we go!")


IEE_EmoEngineEventCreate = libEDK.IEE_EmoEngineEventCreate
IEE_EmoEngineEventCreate.restype = c_void_p
eEvent = IEE_EmoEngineEventCreate()

IS_GetTimeFromStart = libEDK.IS_GetTimeFromStart
IS_GetTimeFromStart.argtypes = [ctypes.c_void_p]
IS_GetTimeFromStart.restype = c_float

IS_GetWirelessSignalStatus = libEDK.IS_GetWirelessSignalStatus
IS_GetWirelessSignalStatus.restype = c_int
IS_GetWirelessSignalStatus.argtypes = [c_void_p]

IS_GetContactQuality = libEDK.IS_GetContactQuality
IS_GetContactQuality.restype = c_int
IS_GetContactQuality.argtypes = [c_void_p, c_int]

IEE_EmoEngineEventGetEmoState = libEDK.IEE_EmoEngineEventGetEmoState
IEE_EmoEngineEventGetEmoState.argtypes = [c_void_p, c_void_p]
IEE_EmoEngineEventGetEmoState.restype = c_int

IEE_EmoStateCreate = libEDK.IEE_EmoStateCreate
IEE_EmoStateCreate.restype = c_void_p
eState = IEE_EmoStateCreate()

meta_channels = ["CMS",
                  "DRL",
                  "FP1",
                  "AF3",
                  "F7",
                  "F3",
                  "FC5",
                  "T7",
                  "P7",
                  "O1", #="IEE_CHAN_Pz"
                  "O2",
                  "P8",
                  "T8",
                  "FC6",
                  "F4",
                  "F8",
                  "AF4",
                  "FP2"]



print("Creating LSL outlet...")
info = StreamInfo('EPOCH', 'FreqBand', 14*5, 10, 'float32', 'uniqueid1234')
meta_sensors = StreamInfo('EPOCH_SENSORS', 'Meta', 18, 2, 'string', 'sensor_uniqueid1234')
meta_battery = StreamInfo('EPOCH_BATTERY', 'Meta', 2, 2, 'float32', 'battery_uniqueid1234')
meta_connection = StreamInfo('EPOCH_CONNECTION', 'Meta', 1, 2, 'string', 'connection_uniqueid1234')


# append some meta-data
channelList = array('I', [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16])
channelListLabel = ["AF3", "F7", "F3", "FC5", "T7", "P7", "O1", "O2", "P8", "T8", "FC6", "F4", "F8", "AF4"]
info.desc().append_child_value("manufacturer", "Emotiv")
channels = info.desc().append_child("channels")
for c in channelListLabel:
    channels.append_child("channel") \
        .append_child_value("label", c + "_theta") \
        .append_child_value("unit", "uV^2/Hz") \
        .append_child_value("type", "frequency")
    channels.append_child("channel") \
        .append_child_value("label", c + "_alpha") \
        .append_child_value("unit", "uV^2/Hz") \
        .append_child_value("type", "frequency")
    channels.append_child("channel") \
        .append_child_value("label", c + "_lowbeta") \
        .append_child_value("unit", "uV^2/Hz") \
        .append_child_value("type", "frequency")
    channels.append_child("channel") \
        .append_child_value("label", c + "_highbeta") \
        .append_child_value("unit", "uV^2/Hz") \
        .append_child_value("type", "frequency")
    channels.append_child("channel") \
        .append_child_value("label", c + "_gamma") \
        .append_child_value("unit", "uV^2/Hz") \
        .append_child_value("type", "frequency")

meta_sensors.desc().append_child_value("manufacturer", "Emotiv")
meta_battery.desc().append_child_value("manufacturer", "Emotiv")
meta_connection.desc().append_child_value("manufacturer", "Emotiv")

sensors = meta_sensors.desc().append_child("sensors")
for sensor in meta_channels:
    sensors.append_child("sensor") \
        .append_child_value("label", sensor) \
        .append_child_value("type", "string")

connection = meta_connection.desc().append_child("connections")
connection.append_child("wireless") \
        .append_child_value("label", "wireless signal strength") \
        .append_child_value("type", "string")

power = meta_battery.desc().append_child("power")
power.append_child("battery") \
    .append_child_value("label", "current") \
    .append_child_value("type", "int")

power.append_child("battery") \
    .append_child_value("label", "max") \
    .append_child_value("type", "int")


# next make an outlet
outlet = StreamOutlet(info)
outlet_sensors = StreamOutlet(meta_sensors)
outlet_battery = StreamOutlet(meta_battery)
outlet_connection = StreamOutlet(meta_connection)


print("done!")


# -------------------------------------------------------------------------

def kbhit():
    ''' Returns True if keyboard character was hit, False otherwise.
    '''
    if sys.platform.startswith('win32'):
        return msvcrt.kbhit()
    else:
        dr, dw, de = select([sys.stdin], [], [], 0)
        return dr != []


# -------------------------------------------------------------------------


userID = c_uint(0)
user = pointer(userID)
ready = 0
state = c_int(0)
systemUpTime = c_float(0.0)

batteryLevel     = c_long(0)
batteryLevelP    = pointer(batteryLevel)
maxBatteryLevel  = c_int(0)
maxBatteryLevelP = pointer(maxBatteryLevel)

systemUpTime     = c_float(0.0)
wirelessStrength = c_int(0)

alphaValue = c_double(0)
low_betaValue = c_double(0)
high_betaValue = c_double(0)
gammaValue = c_double(0)
thetaValue = c_double(0)

alpha = pointer(alphaValue)
low_beta = pointer(low_betaValue)
high_beta = pointer(high_betaValue)
gamma = pointer(gammaValue)
theta = pointer(thetaValue)

IEE_InputChannels_enum = ["IEE_CHAN_CMS",
                          "IEE_CHAN_DRL",
                          "IEE_CHAN_FP1",
                          "IEE_CHAN_AF3",
                          "IEE_CHAN_F7",
                          "IEE_CHAN_F3",
                          "IEE_CHAN_FC5",
                          "IEE_CHAN_T7",
                          "IEE_CHAN_P7",
                          "IEE_CHAN_O1", #="IEE_CHAN_Pz"
                          "IEE_CHAN_O2",
                          "IEE_CHAN_P8",
                          "IEE_CHAN_T8",
                          "IEE_CHAN_FC6",
                          "IEE_CHAN_F4",
                          "IEE_CHAN_F8",
                          "IEE_CHAN_AF4",
                          "IEE_CHAN_FP2"]

IEE_EEG_ContactQuality_enum = ["No Signal",
                               "Very Bad",
                               "Poor",
                               "Fair",
                               "Good"]

IEE_SignalStrength_enum = ["No Signal", "Bad Signal", "Good Signal"]

# -------------------------------------------------------------------------
# -------------------------------------------------------------------------

if libEDK.IEE_EngineConnect(create_string_buffer(b"Emotiv Systems-5")) != 0:
    print("Emotiv Engine start up failed.")
    exit()

metaUpdateTimer = 0

# print "Theta, Alpha, Low_beta, High_beta, Gamma \n"
print ("systemuptime, AF3, AF4, O1, O2")
while (1):

    if kbhit():
        break

    state = libEDK.IEE_EngineGetNextEvent(eEvent)
    # print "after next event %d" % (state)

    if state == 0:
        eventType = libEDK.IEE_EmoEngineEventGetType(eEvent)
        libEDK.IEE_EmoEngineEventGetUserId(eEvent, user)
        if eventType == 16:  # libEDK.IEE_Event_enum.IEE_UserAdded
            ready = 1
            libEDK.IEE_FFTSetWindowingType(userID, 1);  # 1: libEDK.IEE_WindowingTypes_enum.IEE_HAMMING
            # print "User added"

        if ready == 1 and eventType == 64:
            libEDK.IEE_EmoEngineEventGetEmoState(eEvent, eState);
            systemUpTime = IS_GetTimeFromStart(eState);
            wirelessStrength = libEDK.IS_GetWirelessSignalStatus(eState);
            NumContact = libEDK.IS_GetNumContactQualityChannels(eState)
            #print (systemUpTime, ",", end='')
            lsldata = []
            for i in channelList:
                result = c_int(0)
                result = libEDK.IEE_GetAverageBandPowers(userID, i, theta, alpha, low_beta, high_beta, gamma)

                if result == 0:  # EDK_OK
                    #lsldata.append(alphaValue.value)
                    power_data = [thetaValue.value, alphaValue.value,
                                  low_betaValue.value, high_betaValue.value, gammaValue.value]
                    lsldata.extend(power_data)
                    #print ("%.2f" % (alphaValue.value), ",", end='')
                    # print "%.6f, %.6f, %.6f, %.6f, %.6f \n" % (thetaValue.value, alphaValue.value,
                    #                                           low_betaValue.value, high_betaValue.value, gammaValue.value)
                else:
                    #lsldata.append(-1.0)
                    lsldata.extend([0.0, 0.0, 0.0, 0.0, 0.0])
                    #print("XXX", ",", end='')

            #meta information only needs to get sent at 2hz
            if (metaUpdateTimer >= 5):
                metaUpdateTimer = 0
                lslsensors = []
                lslbattery = []
                lslconnection = []

                libEDK.IS_GetBatteryChargeLevel(eState, batteryLevelP, maxBatteryLevelP)

                print("Battery Level:", batteryLevel.value, "out of", maxBatteryLevel.value)
                print("Wireless Strength:", IEE_SignalStrength_enum[wirelessStrength])
                print("Number of Contact:", NumContact)

                if NumContact != 0:
                    for i in range(0, NumContact):
                        SignalCondition = libEDK.IS_GetContactQuality(eState, i)
                        #print IEE_InputChannels_enum[i], "\t :", IEE_EEG_ContactQuality_enum[SignalCondition]
                        lslsensors.append(IEE_EEG_ContactQuality_enum[SignalCondition])
                else:
                    print("Headset signal not available")

                lslbattery.append(batteryLevel.value)
                lslbattery.append(maxBatteryLevel.value)

                lslconnection.append(IEE_SignalStrength_enum[wirelessStrength])

                if len(lslsensors) < 16:
                    for n in range(0, 16-len(lslsensors)):
                        lslsensors.append('None')

                if len(lslbattery) < 2:
                    for n in range(0, 2-len(lslbattery)):
                        lslbattery.append(0)

                if len(lslconnection) < 1:
                    lslconnection.append(0)

                outlet_sensors.push_sample(lslsensors)
                outlet_battery.push_sample(lslbattery)
                outlet_connection.push_sample(lslconnection)
            else:
                metaUpdateTimer += 1

            #print(" ")
            #print(lsldata)
            outlet.push_sample(lsldata)
            
    elif state != 0x0600:

        print("Internal error in Emotiv Engine ! ")
    time.sleep(0.1)

# -------------------------------------------------------------------------
libEDK.IEE_EngineDisconnect()
libEDK.IEE_EmoStateFree(eState)
libEDK.IEE_EmoEngineEventFree(eEvent)
