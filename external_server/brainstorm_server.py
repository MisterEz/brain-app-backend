import socket
import sys
import json
import struct
import socketio
from aiohttp import web
import asyncio
import ssl
import select
import time; 

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

PORT=10000
SECRET='CHANGE_ME' #Obviously change this
MAX_TIMEOUT=10

# Bind the socket to the port
server_address = ('0.0.0.0', PORT)
print('starting up on {} port {}'.format(*server_address))
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(server_address)

# Listen for incoming connection
sock.listen(0)

# Setup Socket.io stuff
sio = socketio.AsyncServer(async_handlers=True)
app = web.Application()
sio.attach(app)

print('waiting for a connection')
connection, client_address = sock.accept()
print('connection from', client_address)

clientPass = connection.recv(len(SECRET)).decode('utf-8')
if (clientPass != SECRET):
    print('Unauthorized connection attempt from ', client_address)
    exit(1)

def recv_msg(sock):
    # Read message length and unpack it into an integer
    raw_msglen = recvall(sock, 4)
    if not raw_msglen:
        return None
    msglen = struct.unpack('>I', raw_msglen)[0]
    # Read the message data
    return recvall(sock, msglen)
def recvall(sock, n):
    # Helper function to recv n bytes or return None if EOF is hit
    data = b''
    while len(data) < n:
        packet = sock.recv(n - len(data))
        if not packet:
            return None
        data += packet
    return data

async def socketLoop():
    lastRecv = time.time()
    while True:
        try:
            ready = select.select([connection], [], [], 0.1)
            if ready[0]:
                msg = recv_msg(connection)
            else:
                msg = None
        except select.error:
            print('connection lost')
            break
        except KeyboardInterrupt:
            print('\nTerminated by user')
            break
        #print(msg.decode('utf-8'))
        if (msg is not None):
            lastRecv = time.time()
            msgDict = json.loads(msg.decode('utf-8'))
            theType = msgDict['type']
            del msgDict['type']
            #print (msgDict)
            await sio.emit(theType, data=msgDict, namespace='/bci')
        else:
            if ((time.time() - lastRecv) > MAX_TIMEOUT):
                print('\nMaximum time without receiving exceeded')
                break
            else:
                await asyncio.sleep(0.1)
    exit(0)
    

futureSocket = asyncio.ensure_future(socketLoop())

async def start_background_tasks(app):
        app['dispatch'] = app.loop.create_task(socketLoop())
 
async def cleanup_background_tasks(app):
    app['dispatch'].cancel()
  
    await app['dispatch']

if __name__ == '__main__':
    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(cleanup_background_tasks)

    ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_context.load_cert_chain('/etc/letsencrypt/live/brainstorm.nm.tc/fullchain.pem', '/etc/letsencrypt/live/brainstorm.nm.tc/privkey.pem')

    web.run_app(app, port=8080, ssl_context=ssl_context)