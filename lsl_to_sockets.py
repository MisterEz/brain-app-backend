from pylsl import StreamInlet, resolve_stream, resolve_byprop, StreamInfo, LostError
from collections import OrderedDict

import socketio

from aiohttp import web
import asyncio
import socket
import threading
import argparse
import json
import struct


'''
    This script will read LSL streams and output websockets for brainstorm on port 8080

    Expected LSL streams:
        EPOCH, type="FreqBand"

    [OPTIONAL]
        EPOCH_SENSORS, type="Meta"
        EPOCH_BATTERY, type="Meta"
        EPOCH_CONNECTION, type="Meta"
'''

# hardcoded sensor order
meta_channels = ["CMS", "DRL", "FP1", "AF3", "F7", "F3", "FC5",
                  "T7", "P7", "O1",  "O2",  "P8", "T8", "FC6",
                  "F4", "F8", "AF4", "FP2"]
PORT=10000
SECRET='CHANGE_ME' #Obviously change this

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description='''Convert an LSL stream to websockets.
Example usage: python3 lsl_to_sockets.py''')

parser.add_argument('-s', '--send', dest='send_to', action='store',
                    default=None,
                    help='also send to an external server')

parser.add_argument('-p', '--port', dest='port_to', action='store',
                    default=PORT,
                    help='external server port')

args = parser.parse_args()

#print(args.send_to)
can_send_external = (args.send_to is not None)
if (can_send_external):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Connect the socket to the port where the server is listening
    server_address = (args.send_to, args.port_to)
    print('connecting to {} port {}'.format(*server_address))
    sock.connect(server_address)
    sock.sendall(SECRET.encode('utf-8'))



print("Attempting to resolve FreqBand LSL stream")
streams = []

while len(streams) < 1:
    print('.')
    streams = resolve_byprop('type', 'FreqBand', timeout=1)



print("Attempting to resolve META LSL streams")
meta_streams    = resolve_byprop("type", "Meta", minimum=3, timeout=5)
if (len(meta_streams) < 3):
    print("WARNING: Failed to find a META LSL stream.")


# streams[0] will be the EEG data as follows
'''
    <channels>
        <channel>
            <label>AF3_theta</label>
            <unit>uV^2/Hz</unit>
            <type>frequency</type>
        </channel>
        <channel>
            <label>AF3_alpha</label>
            <unit>uV^2/Hz</unit>
            <type>frequency</type>
        </channel>
        ...
    </channels>
'''
inlet = StreamInlet(streams[0])
info = inlet.info()
print(info.as_xml())

# meta_streams have a mixed structure
# This is poor practice, do not copy this
'''
    <sensors>
        <sensor>
            <label>AF3</label>
            <type>string</type>
        </sensor>
        <sensor>
            <label>F7</label>
            <type>string</type>
        </sensor>
        ...
    </sensors>
    [OR]
    <connections>
        <wireless>
            <label>Wireless signal strength</label>
            <type>string</type>
        </wireless>
    </connections>
    [OR]
    <power>
        <battery>
            <label>current</label>
            <type>int</type>
        </battery>
        <battery>
            <label>max</label>
            <type>int</type>
        </battery>
    </power>
'''
battery_inlet = None
sensors_inlet = None
connection_inlet = None

battery_info = None
sensors_info = None
connection_info = None

for stream in meta_streams:
    temp_inlet = StreamInlet(stream)
    temp_info = temp_inlet.info()
    #print (temp_info.as_xml())
    toplevel = temp_info.desc().last_child().name()
    if (toplevel == "power"):
        battery_inlet = temp_inlet
        battery_info = temp_info

    elif (toplevel == "sensors"):
        sensors_inlet = temp_inlet
        battery_info = temp_info

    elif (toplevel == "connections"):
        connection_inlet = temp_inlet
        connection_info = temp_info

    else:
        print("ERROR: unknown meta type: " + toplevel)
        exit(0)



''' Make a dictionary of the channel structure as follows
sensors = {
    'AF3': {
        'alpha': 0.0,
        'theta': 0.0,
        ...
    },
    ...
}
'''
sensors = OrderedDict()
channelIdxMap = []

ch = info.desc().child("channels").child("channel")
for k in range(info.channel_count()):
    lbl = ch.child_value("label")
    parts = lbl.split('_')
    if parts[0] not in sensors:
        sensors[parts[0]] = OrderedDict()
    sensors[parts[0]][parts[1]] = 0.0
    channelIdxMap.append((parts[0], parts[1]))
    ch = ch.next_sibling()


async def send_external(data, theType):
    newData = dict(data)
    newData['type'] = theType
    msg = json.dumps(newData).encode('utf-8')
    msg = struct.pack('>L', len(msg)) + msg
    #print(msg)
    sock.sendall(msg)


# Setup Socket.io stuff
sio = socketio.AsyncServer()
app = web.Application()
sio.attach(app)

async def bciSocketLoop():
    try:
        while True:
            try:
                sample, timestamp = inlet.pull_sample(0)
            except LostError:
                print('Stream ended')
                break
            except KeyboardInterrupt:
                print('\nTerminated by user')
                break

            if (sample is not None):        
                for idx, channel in enumerate(sample):
                    loc = channelIdxMap[idx]
                    sensors[loc[0]][loc[1]] = channel

                if (can_send_external):
                    await send_external(sensors, 'bciUpdate')
                await sio.emit('bciUpdate', data=sensors, namespace='/bci')
            else:
                await asyncio.sleep(0.05) # this lets things happen in the background btw
    except asyncio.CancelledError:
        pass

async def metaSocketLoop(metaType):
    theData = {}
    theInlet = None
    if (metaType == "battery"):
        theInlet = battery_inlet
    elif (metaType == "sensors"):
        theInlet = sensors_inlet
    elif (metaType == "connection"):
        theInlet = connection_inlet
    else:
        print("ERROR unknown metatype: " + metaType)
        return

    try:
        while True:
            try:
                sample, timestamp = theInlet.pull_sample(0)
            except LostError:
                print('Meta Stream ended: ' + metaType)
                break
            except KeyboardInterrupt:
                print('\nTerminated by user: ' + metaType)
                break

            if (sample is not None):    
                if (metaType == "sensors"):
                    for idx, data in enumerate(sample):
                        channel = meta_channels[idx]

                        theData[channel] = data
                elif (metaType == "battery"):
                    theData = {'batteryLevel': sample[0], 'maxBatteryLevel': sample[1]}
                else: #connection
                    theData = {'signalStrength': sample[0]}
                
                if (can_send_external):
                    await send_external(theData, metaType + 'Update')
                await sio.emit(metaType + 'Update', data=theData, namespace='/bci')
            else:
                await asyncio.sleep(0.05) 
    except asyncio.CancelledError:
        pass

futureBci = asyncio.ensure_future(bciSocketLoop())
futureSensors = asyncio.ensure_future(metaSocketLoop('sensors'))
futureBattery = asyncio.ensure_future(metaSocketLoop('battery'))
futureConnection = asyncio.ensure_future(metaSocketLoop('connection'))

async def start_background_tasks(app):
        app['dispatch'] = app.loop.create_task(bciSocketLoop())
        app['sensors'] = app.loop.create_task(metaSocketLoop('sensors'))
        app['battery'] = app.loop.create_task(metaSocketLoop('battery'))
        app['connection'] = app.loop.create_task(metaSocketLoop('connection'))
 
async def cleanup_background_tasks(app):
    app['dispatch'].cancel()
    app['sensors'].cancel()
    app['battery'].cancel()
    app['connection'].cancel()

    await app['dispatch']
    await app['sensors']
    await app['battery']
    await app['connection']

    if (can_send_external):
        print('closing socket')
        sock.close()

if __name__ == '__main__':
    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(cleanup_background_tasks)
    web.run_app(app, port=8080)
