## Brain App Backend Tools

Backend tools for [Brainstorm](https://gitlab.com/MisterEz/brain-app)  utilizing [LSL](https://github.com/sccn/labstreaminglayer) & the [Emotiv community-sdk](https://github.com/Emotiv/community-sdk).  Probably not reusable for any other projects, might be useful as a reference for someone.


### Emotiv SDK to LSL
Pulls data from an Emotiv headset and converts it to a LSL streams. 

**Usage:**
`$ python3 emotiv-sdk-to_lsl.py`

### LSL to sockets
Searches for the LSL streams from emotiv-sdk-to-lsl and makes socket.io websockets. 

**Usage**
`$ python3 lsl_to_sockets.py`

### Dependencies

 **Python >= 3.4**
 The following packages
[pylsl](https://pypi.org/project/pylsl/)
[python-socketio](https://pypi.org/project/python-socketio/)
[aiohttp](https://pypi.org/project/aiohttp/)

Get them all with
```
$ pip3 install --user pylsl python-socketio aiohttp
```

**Emotiv SDK binaries**
Get from [here](https://github.com/Emotiv/community-sdk) or get just the binaries with the following
```
svn checkout https://github.com/Emotiv/community-sdk/trunk/bin
```
Directory structure should be like this 
```
brain-app-backend/
    bin/
        ...
    emotiv-sdk-to_lsl.py
    lsl_to_sockets.py
```